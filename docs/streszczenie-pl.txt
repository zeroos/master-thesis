
W dzisiejszych czasach bardzo łatwo stworzyć duże zbiory nagrań dźwięku. Jednak
wciąż nie mamy dostępu do technologi, która umożliwiałaby ich analizę. Takie
dane byłyby znacznie użyteczne, gdybyśmy mogli w nich łatwo wyszukiwać wystąpień
różnych fraz. Ta praca poświęcona jest rozwiązaniu tego problemu za pomocą
wyrażeń regularnych. Korzystając z nich, dostosowujemy wagę poszukiwanych fraz
tak, aby prawdopodobieństwo ich znalezienia było większe. Dzięki temu możemy
dobrać próg, przy którym wyrażenie jest rozpoznawane.
