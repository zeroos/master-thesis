% vim:ft=tex:
%

In this chapter we will introduce experiments undertaken during the work on this
thesis. Section~\ref{sec:exp2:datasets} is devoted to describe the datasets that
our solution was tested on. Section~\ref{sec:exp2:desc} outlines our approach to
the experiments, their detailed implementation and results.

\section{Datasets}\label{sec:exp2:datasets}

During the work on this thesis we have conducted multiple practical experiments
in order to test whether our assumptions are correct. At the beginning they
mostly involved testing different versions of $R$ graphs and manually examining
the outcome on different datasets.

\subsection{Digits dataset}

We have even prepared a special dataset, named ``Digits''. It contained over $7$
minutes of recordings created by Paweł Florczuk and Michał Barciś. Both speakers
were pronouncing random digits in two ways: fast and slow. Resulting sets were
tagged with ``hard'' and ``easy'' labels. This data has two important properties
that made it ideal to start the research with.

\begin{itemize}
    \item It is relatively small. Thanks to that all experiments were finished
        quickly and the results were easy to analyse. 
    \item There are only ten words. It makes the data easier to decode and thus
        provides better results, which are desirable at the early stages.
\end{itemize}

After achieving satisfactory outcomes on this dataset with a language model
containing ten words we decided to make it a bit harder. We did not want to jump
straight into a big dataset, but wanted to test how our solution would work with
larger graphs. Therefore, we decided to use a bigger language model. Instead of
recognizing actual words, they have been replaced by phonemes, i.e.\ now we will
receive results in a form of ``d w a j e d e n p j e ni c'' in place of ``dwa
jeden pięć''. That way, the much bigger graph has been constructed, but the test
data was still pretty simple. In order to make the experiment more realistic,
instead of assigning uniform probabilities to phonemes, we constructed a bigram
language model on them. This way we have faced a slightly harder problem, but it
was still possible to assess it manually. Each experiment also run relatively
fast, it took around ten minutes to decode the whole dataset. It made working
with this data much quicker and efficient.

\subsection{Experts}

The second dataset we have used is named ``Experts''. It is one of the most
often used test sets in the \textit{Audioscope} project. It consists of $2$
hours and $22$ minutes of audio recordings made probably in a school. Speakers
usually explain some secondary-level subjects. Recordings are divided into
$1857$ utterances, each of them are a couple of seconds long and correspond to
around one sentence.

It is not possible to analyse such big dataset manually. Therefore, we have
designed a test that allows us to easily asses the effectiveness of our
solution. It is not tailored in any way to our approach. In fact, it was
proposed, even before this project started, as a generic way to rate solutions
to a problem of searching phrases in audio data.

The test contains $1863$ search patterns represented as a base form together
with a set of related words. For example, base form \textit{bohater} (``hero''
in Polish) is related to $10$ words: \textit{bohater}, \textit{bohatera},
\textit{bohaterami}, \textit{bohaterom}, \textit{bohaterowi},
\textit{bohaterowie}, \textit{bohaterów}, \textit{bohaterem}, \textit{bohaterze}
and \textit{bohaterzy}. For each of the records in the ``Experts'' dataset it is
marked whether it contains each of the search patterns or not. The information
is only boolean, the place where the phrase is found is not marked. The goal of
this test is just to tag each recording with all found patterns.

\section{Experiments description and results}\label{sec:exp2:desc}

Our approach to the test introduced in the previous section, is based on the
idea described in Chapter~\ref{sec:exp}. We will try to find each phrase
individually during decoding and mark them in the output data with a unique
symbol \textit{<re\_found>}. The algorithm would be realised by lowering the
weights of the searched patterns in the language model. We achieve it by
composing regular expressions with the \textit{HCLG} graph.

\begin{sidewaysfigure}[htp]
    \centering
    \includegraphics[width=\textwidth]{img/experiments_regexp_example.png}
    \caption{An example of an \textit{FST} representing a regular expression
    used in process of speech recognition.}
\label{fig:exp2:exp_re_exmpl}
\end{sidewaysfigure}

In the figure~\ref{fig:exp2:exp_re_exmpl} an example of transducer used as a
search pattern for base form ``idea'' is presented. The weights of arcs in the
phrase are set to $-3$, but some pre computations have been made in order to
push weights as close as possible to the starting node, on all paths. All found
occurrences of the expression are marked with upper-case letters.

\begin{example}\label{exmpl:exp2:decoding} \hfill \\
\noindent A decoding of the recording transcribed as \\ 
\\
``kolejne wartości kolejne \textbf{idea}ły które miejmy nadzieję że będą
przy świecać poszczególnym pokoleniom'' 
\\
\noindent with the \texttt{R} graph presented in
the figure~\ref{fig:exp2:exp_re_exmpl} is:\\
\\
\noindent ``p a l e j n e w a r a t o si ci i k o l e ni a I D E A <re\_found> ll e p ll n e
m j e n a dzi e rz e w e n a m p sz y si f j e c a ci p o c z e b o l e m p o k
o l e ni o''.
\end{example}

In the example~\ref{exmpl:exp2:decoding} an actual decoding is presented. As we
can see, the searched pattern (``idea'') was found. The result might not have
been exactly what we have been looking for, because it is a part of a different
word. But it gives a good intuition of the construction of the whole system. In
fact, in the current setup, it is not possible to distinguish such false
positives, because we have no notion of a word. In the Chapter~\ref{sec:concl}
we will give some ideas how this approach could be improved.

Our goal is to plot the \textit{receiver operating characteristic}
(\textit{ROC}) diagram of the classifier. It illustrates the true positive rate
against the false positive rate on the test dataset for different \texttt{R}
graph's weights. For the reasons described in
Section~\ref{sec:exp:solution:choosing_weight}, this diagram is suitable to
present the efficiency of our solution. However, in order to compare it with
other programs, we computed the \textit{F1 score} for each point in the
\textit{ROC} diagram and chose the highest value.

The ``Experts'' dataset is huge. Specifically, there is a lot of patterns we
have to search for. Our solution was not designed to serve this purpose. We
mainly aimed at the highest effectiveness and the ability to easily control the
ratios of false and true positives. Therefore, this test was impractical for our
solution. It would take more than two months on the available hardware to
produce all needed results. We decided not to run the whole test, but just
approximate its result.

In order to draw the \textit{ROC} diagram we need two parameters: \textit{false
positive rate} (\textit{FPR}) and \textit{true positive rate} (\textit{TPR}).
Due to the nature of data, where most of the results are negative, it is fairly
easy to approximate the false positive rate precisely. In order to estimate both
rates we have determined their confidence intervals. In order to achieve that we
have used the \textit{Wilson} method~\parencite{wallis_binomial_2013} for
binomial distribution. 

\begin{figure}[htp]
    \centering
    \includegraphics[width=0.75\textwidth]{img/roc.png}
    \caption{ROC diagram of the classifier implemented using presented concepts.}
\label{fig:exp2:roc}
\end{figure}

In the figure~\ref{fig:exp2:roc} the \textit{ROC} diagram of our classifier is
presented. The right part of it stands out from the rest. For a false positive
rate greater than $0.2$ the true positive rate often drops for a lower weight
value and overall the results seem random. This is due to the way the decoder in
\textit{Kaldi} works. 

\begin{example}
    Let us consider a case when we want to find all occurrences of the phoneme
    sequence ``\myregexp{P O SZ CZ E G U L N Y M}'' in the test data, with a
    very low weight (e.g. $-5$). The decoder in \textit{Kaldi} does not always
    compute the exact result, because it is very costly. It just greedly
    considers some number of the best options. Because the cost of our search
    phrase is so low, it tries hard to fit it to the data. It does not ``know''
    how long each phoneme is, so it might for example consume even a couple of
    original words and recognize them as the single phoneme \myregexp{P}. Then,
    the same thing can happen with the phoneme \myregexp{O}, etc. At some point,
    the recording finishes, but all the options that the decoder is considering,
    might be in states that are somewhere in the middle of the search phrase. 
    
    None of them is a final state, so no result should be returned. However, in
    practice, if the decoder cannot find a path that ends with a final state, it
    just returns its best guess. Unfortunately, because only a fraction of the
    search pattern was found, it is not marked as a positive classification.

\label{exmpl:exp2:roc_phenomena}
\end{example}

\begin{figure}[htp]
    \centering
    \includegraphics[width=0.75\textwidth]{img/roc-zoom.png}
    \caption{ROC diagram for values of \texttt{FPR} between $0$ and $0.1$.}
\label{fig:exp2:roc-zoom}
\end{figure}

Fortunately, it is not a big problem for us. The value of false positive rate
greater than $0.2$ means, that in each fifth phrase, the search pattern would be
found. In reality, they occur much less frequently. So a program that has such
high \texttt{FPR} would be impractical. 

The most interesting portion of the ROC diagram is presented in the
figure~\ref{fig:exp2:roc-zoom}. It shows only results with false positive rate
lower than $10\%$. The first classifier (with weight set to $0$) has a
\texttt{TPR} of about $5\%$ with \texttt{FPR} close to $0$ ($0.01\%$). Then,
each subsequent program with a lower weight value achieves greater true positive
rates, for a cost of a false positive rate.
