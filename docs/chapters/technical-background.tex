% vim:ft=tex:
%

In the following chapter we will introduce software that was used in order to
create this thesis. Section~\ref{sec:tech:kaldi} describes the \textit{Kaldi}
toolkit and \textit{OpenFST}. Both of them are used by us in our speech
recognition pipeline. In the next section (\ref{sec:tech:audioscope}) a bigger 
project that this thesis is a part of, is introduced.
Section~\ref{sec:tech:audioup} describes the application made in order to gather
more data. At the end, in Section~\ref{sec:tech:utilities}, programs and
libraries used by us are presented.

\section{Kaldi and OpenFST}\label{sec:tech:kaldi}

\textit{Kaldi} \parencite{Povey11thekaldi} is a speech recognition toolkit
written mainly in \textit{C++} and \textit{Bash}. It is licensed under the
\textit{Apache License v2.0}, which means that it is free for use and edit, as
long as the original work is cited. \textit{Kaldi} is intended to be used by
researchers, therefore it is easily modifiable and extendable. In practice, it
is a set of small programs and \textit{Bash} scripts that allow us to construct
a working speech recognition pipeline.

\textit{Kaldi} is strongly integrated with the \textit{OpenFst} toolkit
\parencite{openfst}. This library implements a way to construct the
\textit{weighted finite-state transducers} together with basic operations on
them. It is designed with the efficiency in mind. Additionally, some utility
programs are provided in order to analyse and process transducers, which makes
the work with this library much more pleasurable.

\section{Project AudioScope}\label{sec:tech:audioscope}

This thesis is a part of a larger project, named \texitit{AudioScope}.  The aim
of this project is to create an automatic search system, working on audio files.
It is developed by a science consortium formed by the \textit{University of
Wrocław}, the \textit{Wrocław University of Science and Technology} and the
\textit{Neurosoft} company. The project started at the 1st of April 2015 and is
due to finish at the 31st of March 2017.

The project is funded by the \textit{Applied Research Programme} of the
\textit{National Centre for Research and Development}. It supports various
science institutions and industries in their practical studies.

The system receives as an input a set of speech recordings in Polish.  It should
mark all fragments that contain a searched phrase. In order to achieve the
goals, we have decided to use the \textit{Kaldi} toolkit, with various parts
modified by different teams and people. The team at the \textit{University of
Wrocław} mainly focuses its work on the \textit{Language Models} and data
acquisition.

Essential datasets in Polish are very hard to get. We have struggled to acquire
both, good corpora and audio recordings with transcriptions. We did not limit
ourselves to just one corpus or one dataset. The decision has been made to find
as any different sources as possible. Later, we merge collected data together
and run tests on different combinations.

In terms of corpora, one of the best sets that we were able to find, is the
National Corpus of Polish (NKJP, \texttt{http://nkjp.pl}) dataset. It was
created at the \textit{University of Łódź} and published for everyone to use. We
have also made a couple of corpora on our own. Among others, one was created from
the transcriptions of audio files that we acquired, another from
\textit{Wikipedia}. 

One of the recurring problems, that was often mentioned on our meetings, was
where to find datasets of transcribed audio files. In the beginning, we have
started with a collection of audiobooks. It was a great starter, because
hundreds of hours of data could be easily found. Unfortunately, the way speakers
were reading the text was far from natural. We have also used some smaller sets
of audio files and even tried to utilise some recordings in different languages.

\section{Audioup}\label{sec:tech:audioup}

\begin{figure}[htp]
  \centering
  \includegraphics[width=.7\textwidth]{img/audioup1.png}
  \caption{A screenshot of the \textit{Audioup} program showing the introduction screen.}
\label{fig:tech:audioup1}
\end{figure}%
\begin{figure}[htp]
  \centering
  \includegraphics[width=.7\linewidth]{img/audioup2.png}
  \caption{A screenshot of the \textit{Audioup} program showing the screen displayed just
  before starting recording.}
\label{fig:tech:audioup2}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=.7\linewidth]{img/audioup3.png}
  \caption{A screenshot of the \textit{Audioup} program showing the screen displayed after
  sending the recording to the server.}
\label{fig:tech:audioup3}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=.9\linewidth]{img/audioup_meta.png}
  \caption{A form to input additional details in the \textit{Audioup} program. }
\label{fig:tech:audioup_meta}
\end{figure}

Because it is so hard to acquire transcribed audio recordings in Polish, we
decided to make an application that will allow us to create such kind of data. It
is called \textit{Audioup}. It allows people to easily record themselves reading
some predefined texts. We have chosen fragments from the
\textit{Winnie-the-Pooh} book. It is easy to read, interesting for children as
well as adults and does not contain difficult language. The application is
working as a web page and utilizes the newest technologies in order to capture
voice, encode it and send it to the server, without the use of any plug-ins or
additional programs. It was written in \textit{HTML5} and \textit{JavaScript}.
It uses technologies such as \textit{application cache}, \textit{local storage},
\textit{audiocontext}.

It was designed with the ease of use as the priority. Only three clicks of the
mouse are needed to start recording. The text on the site was also reduced to
the minimum. Additionally, the web page is very accessible. Its responsive
design makes it usable on a variety of devices like smartphones, tablets and
computers. 

In order to make the recording possible almost everywhere, the first time the
web page is visited, it stores itself on a guest's computer. After that, it is
available to use and record audio even when there is no Internet connection. The
data is temporarily stored in the browser until it could be sent to the server. 

In the figures~\ref{fig:tech:audioup1},~\ref{fig:tech:audioup2}
an~\ref{fig:tech:audioup3} a basic usage of the application is presented. At the
beginning a user is greeted with instructions how to use the application. After
clicking the ``start'' button they are presented a text to read aloud. They
start the recording and read the text. Then the gathered data would be automatically
sent to the server.  The user can then listen to it or download it and start the
process again.

The figure~\ref{fig:tech:audioup_meta} presents a form used to give some
additional information about the speaker. They are stored together with the
recorded data and the transcriptions.

\section{Utilities}\label{sec:tech:utilities}

The project also utilised a lot of small scripts and simple programs in order to
achieve its goal. Most of them were written in \textit{Python} and
\textit{Bash}. The results of experiments were gathered and processed using
\textit{Jupyter notebook} \parencite{PER-GRA:2007}. Diagrams were potted with
\textit{Matplotlib} \parencite{Hunter:2007}. Graphs were created using
\textit{TikZ} \LaTeX\ library \parencite{tantau:2013a} and the \textit{Linux}
\textit{dot} utility.
