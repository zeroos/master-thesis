% vim:ft=tex:
%


\section{Motivation}\label{sec:intro:motivation}
Nowadays a magnitude of digitized audio data exists and is easily available. The
first time a person was able to record his voice was in 1857, when a Parisian
inventor Édouard-Lon Scott de Martinville patented his invention --- the
phonautograph \parencite{firstsoundsorg}. Since then, a massive development in
the area of audio recording has been made. Currently almost everyone carries a
smartphone on himself, capable of recording sound with exceptional quality.
Additionally, the size of recording devices and microphones was reduced so much,
that it is possible to record almost everything, everywhere and at all times.

In fact, there is a lot of places where hundreds of hours of speech recordings
are being produced every day. Below we will present some examples of such
places.

\begin{itemize}
    \item In \textbf{courtrooms}, the testimonies are usually recorded.
        Sometimes a court stenographer takes part in trials and his role is to
        transcribe spoken or recorded speech into a written form. However, he is
        rarely present in the lower court.
    \item In \textbf{call centres} the calls are recorded and archived for
        future inspection.
    \item In some companies it is common to \textbf{record meetings} in order to
        keep notes of the decisions that have been made.
    \item \textbf{Detectives} and \textbf{law enforcement} sometimes use
        concealed microphones or wire phones to spy on people.
\end{itemize}


In all the above cases hundreds, or even thousands of hours of recordings are
being gathered. This makes manual inspection of this data highly inefficient and
thus impractical. As a result, the true potential of those recordings is not
being exploited. Just think how many options we would have if it was possible to
search for any information hidden in this data automatically.

\begin{itemize}
    \item It would be possible to search for example for a name of a suspect or
        for all mentions of the word ``robbery'' in the testimony. Evidence
        would be harder to miss and, most importantly, people would spend much
        less time analysing the data.
    \item In call centres the ability to search for some predefined phrases
        would make analysis of data much simpler. It would be possible for
        example to create statistics on how often people asked about particular
        features, etc.
    \item Companies could analyse recordings from their meetings and decide
        almost instantly whether some topic has been discussed and what
        decisions were made.  
    \item Wiretapped data could be analysed automatically and suspicious
        recordings could be tagged --- for example the ones that mention bombs
        or guns.
\end{itemize}


\section{Thesis objectives}

This work is a part of a project named \textit{AudiosScope}, described in
the Section~\ref{sec:tech:audioscope}. The main goals for this thesis are:

\begin{enumerate}
    \item present a problem of searching for patterns in audio data,
    \item introduce a solution to this problem, that is based on speech
        recognition,
    \item gather in one place all information essential to understand this
        solution,
    \item provide ideas about additional work that may be done.
\end{enumerate}


\section{Thesis organization}

This thesis is divided into chapters. Each one of them provides some additional
knowledge and utilises concepts introduced earlier in this study.

In the rest of the Chapter~\ref{sec:intro} we introduce the problem and briefly
state the goals of this thesis. Chapters~\ref{sec:sci} and~\ref{sec:tech}
explain theoretical knowledge needed to understand concepts utilised later and
describe programs useful during the work on this project.

The main idea of this thesis is presented in Chapter~\ref{sec:exp}. Then, in
Chapter~\ref{sec:exp2} some experiments using this idea are outlined together
with practical usage.


\section{Problem formulation}\label{sec:intro:problem}

We are trying to solve the problem of searching for a pattern in an audio file.
Below we will try to roughly specify it.


\begin{definition}\label{def:exp:problem}\hfill\\
\noindent\textbf{Input:}

\begin{itemize}
    \item An audio file containing a fragment of speech data, usually a sentence.
    \item A text file containing a pattern to search for.
\end{itemize}

\noindent\textbf{Output:}

A decision whether the given pattern was pronounced in the audio file or not.
\end{definition}


\begin{figure}[htp]
  \centering
  \includegraphics[width=\textwidth]{img/wav.png}
  \caption{A waveform representing an example of speech recording}
\label{fig:intro:wav}
\end{figure}

The problem is not easy to solve. In the figure~\ref{fig:intro:wav} an example
of audio recording is given, with areas marked with a darker background. They
represent the same phrase pronounced in different manners. Even on this waveform
we can see how distinct they are. Some are short, others are very long.  Some
have small amplitude, some have very high amplitude. The dynamics also change a
lot in each pronunciation. 

This is only an example, but it shows how complex the problem is. Therefore, in
order to solve it, we decided to use the most modern approaches from the field
of \textit{natural language processing} and \textit{machine learning} (described
in Chapters~\ref{sec:sci} and~\ref{sec:tech}) and modify an existing speech
analysis software.


%\section{Prior research}
%
%Our interests in \textit{natural language processing} started almost two years
%ago. The first project was a speech synthesizer. It let us build a solid knowledge
%base of different phenomena associated with words pronunciation and a generic
%understanding of natural languages.
%
%After that we decided to realize a project aiming at speaker detection. The
%final solution could tag a recording with a name of a talker, or just his
%gender, if his voice samples were not present in the database. 
