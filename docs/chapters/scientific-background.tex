\section{Regular expressions}

In the following section we are going to introduce \textit{regular expressions}
as an easy way to define a search query. Nowadays, they are very popular and
used in various places --- ranging from lexical analysers and text editors even
to web crawlers. 

The aim of this section is to formalize those regular expressions and define a
simple notation. In the beginning, we will show some operations on languages
that can be represented by them. Then, we will introduce the inductive
definition of these expressions. In the end, we will show how to convert them to
non-deterministic automata and weighted finite state transducers.

\subsection{Regular languages}\label{sec:sci:regular-languages}

Regular expressions are used to represent \textit{regular languages}. But what
exactly is a \textit{regular language}? First, we need to define a 
\textit{language} (or, more precisely, a \textit{formal language}). It is a
(perhaps infinite) set of \textit{words} over a given \textit{alphabet} 
$\Sigma$. For example, we can define a language $L = \{abc, cba\}$ over the 
alphabet $\Sigma = \{a, b, c\}$. Often we will not state the alphabet explicitly.
This knowledge should be sufficient for understanding the rest of this chapter.
For more information about this exciting field of knowledge, please refer to the
handbook about formal languages, for example 
\parencite{hopcroft_introduction_1979}.

We will now define some operations on the languages that are going to be later 
represented with regular expressions. In the following examples both $L$ and 
$R$ are any languages. All the following operations return a new language.

\begin{description}
    \item[concatenation]
        The concatenation of $R$ and $L$ contains all strings that can be formed
        by taking a string from $L$ and appending to it a string from $R$.
        
        \begin{example}
            Let $R = \{a,b\}$, $L = \{c,d\}$. Then the concatenation of $R$ 
            and $L$ equals $\{ac, ad, bc, bd\}$.
        \end{example}


    \item[union]
        The union of $L$ and $R$ contains all strings from $L$ and all strings
        from $R$.
    
        \begin{example}
            Let $R = \{a,b\}$, $L = \{c,d\}$. Then the union of $R$ and $L$
            equals $\{a,b,c,d\}$.
        \end{example}

        \begin{example}
            Let $R = \{a,b\}$, $L = \{a,d\}$. Then the union of $R$ and $L$
            equals $\{a,b,d\}$.
        \end{example}

    \item[Kleene star]
        The Kleene star of $L$ contains an empty string and all strings from $L$
        concatenated with the Kleene star of $L$. In other words, it contains
        all variable-length variations with repetitions of all strings from $L$.

        \begin{example}
            Let $L = \{a,b\}$. The Kleene star of $L$ equals to an infinite 
            language $\{\varepsilon,a,b,aa,ab,ba,bb,aaa,aab,aba,abb\dots\}$.
        \end{example}

\end{description}

\subsection{The structure of regular expressions}

Regular expressions define languages. Let $r$ be an expression. Notation $L(r)$
denotes a language represented by $r$.

Now it is time to introduce the inductive definition of regular expressions.
We will start with the \textbf{basis}:
 
\begin{definition}[regular expressions]\label{def:regular-expressions}\hfill

\begin{itemize}
    \item Both $\varepsilon$ and $\emptyset$ are regular expressions. The first one
        denotes the language with only one word of length $0$. 
        The second one denotes an empty language.
        \begin{example}
            $L(\varepsilon) = \{\varepsilon\}$, $L(\emptyset) = \emptyset$.
        \end{example}
    \item Any symbol from the alphabet $\Sigma$ is a regular expression. It
        denotes a language containing only this symbol.
        \begin{example}
            $L(a) = \{a\}$.
        \end{example}
\end{itemize}

Let us proceed with the inductive definition. It has four constructors. The
first three correspond to the operations on languages defined in
Section~\ref{sec:sci:regular-languages}. The fourth one introduces parentheses.

\begin{itemize}
    \item If $A$ and $B$ are regular expressions, then $AB$ is a regular
        expression denoting the concatenation of $L(A)$ and $L(B)$.

        \begin{example}
            Regular expression \myregexp{abc} denotes language $\{abc\}$.
        \end{example}
    \item If $A$ and $B$ are regular expressions, then $A|B$ is a regular
        expression denoting the union of $L(A)$ and $L(B)$.
        \begin{example}
            Regular expression $a|b$ denotes language $\{a,b\}$.
        \end{example}
    \item If $A$ is a regular expression, then $A^*$ is a regular expression
        denoting the Kleene star of $A$.
        \begin{example}
            Regular expression $a^*$ denotes an infinite language 
            $\{\varepsilon, a, aa, aaa, aaaa\dots\}$.
        \end{example}
    \item If $A$ is a regular expression, then $(A)$ is also a regular
        expression denoting the same language. $L(A) = L(\left( A \right))$.
\end{itemize}
\end{definition}


\subsection{Extensions to regular expressions}

The definition~\ref{def:regular-expressions} gives us the complete tool to
denote regular languages. Unfortunately, in practice, this basic set is not
very expressive. In order to make it more useful we do need to introduce some
syntactic sugar.

Since we will use the regular expressions as a tool to specify search queries,
we may often say that the regular expression \textit{matches} some text.

\begin{definition}
    Regular expression $A$ matches string $T$ $\iff$ $T \in L(A)$.
\end{definition}

\begin{example}
    Regular expression $eve|adam$ matches $adam$. This expression also matches
    $eve$ and these are the only two strings that it matches.
\end{example}


\begin{itemize}
    \item \myregexp{.} is a regular expression that denotes a language with all symbols 
        from $\Sigma$.
        \begin{example}
            If $\Sigma = \{a,b,c\}$, then $L(.) = L(a|b|c) = \{a,b,c\}$.
        \end{example}
    \item \myregexp{[A]}, where $A$ is a string with different symbols, is a
        regular expression that denotes a language with all symbols from $A$.
        \begin{example}
            $L([xy]) = L(x|y) = \{x,y\}$.
        \end{example}
    \item If $A$ is a regular expression, then~\myregexp{A+} is a regular 
        expression that equals~\myregexp{AA*}, i.e.\ it denotes almost the same
        language as~\myregexp{A*}, but without the empty string~$\varepsilon$.
\end{itemize}


\begin{example} \hfill
    \myregexp{http://.+/[ABC][123]} matches strings \myregexp{http://t/A1},
    \myregexp{http://example.com/C3}, etc., but it does not match 
    \myregexp{/C3} or \myregexp{http:///C3}.
\end{example}


\begin{example}
    \myregexp{(Pawn|Queen) on [ABC][0123]+} matches strings 
    \myregexp{Pawn on B0}, \myregexp{Queen on A12}, etc., 
    but it does not match \myregexp{Pawn on A} or 
    \myregexp{Pawn on 0}.
\end{example}

Regular expressions are a perfect tool to provide the search queries one may
look for in the audio files. They are not only very expressive but also easy to
use.

\section{Weighted Finite State Transducers}\label{sec:sci:wfst}

In the following section we are going to introduce the 
\textit{Weighted Finite State Transducers (WFSTs)}, which are widely 
used in the state-of-the-art automatic speech recognition systems 
\parencite{Mohri:1997:FTL:972695.972698,mohri2008speech} as well as 
in our work.

In Section~\ref{sec:sci:wfst:fsa} we will introduce a \textit{finite state
automata} (or \textit{finite state machines}) with non-determinism in
Section~\ref{sec:sci:wfst:nfa}.  Then, Section~\ref{sec:sci:wfst:fst} will
modify them in order to define \textit{finite state transducers}, which will
finally be expanded in Section~\ref{sec:sci:wfst:wfst} to \textit{weighted
finite state transducers}.


\subsection{Finite state automata}\label{sec:sci:wfst:fsa}

A finite state automaton is an abstract device that accepts or rejects finite
strings. It usually operates on a predefined alphabet $\Sigma$. One can think of
such automaton as a machine that is in some state. It can read symbols from the
input tape (string), which can change the state it is in. This abstract machine
also has an abstract light that turns on when it is in an accepting state or off
when it is not.

More formally, the \textit{finite state automaton} is a 5-tuple, $(Q, \Sigma,
\delta, q_0, F)$, where:

\begin{itemize}
    \item $Q$ is a finite set of all states,
    \item $\Sigma$ is a finite alphabet, i.e.\ a set of input symbols,
    \item $\delta$ ($\delta : Q \times \Sigma \rightarrow Q$) is a transition
        function,
    \item $q_0$ is an initial state,
    \item $F$ is a set of accepting states.
\end{itemize}

The machine works as follows: it starts in the initial state $q_0$. It reads a
symbol~$s$ from the input tape and changes the state to $\delta(q_0, s)$. Those
operations are being repeated until there is nothing new to read. The machine is
then in some state $Q_l$. If $Q_l \in F$, the string is being accepted.
Otherwise, it is rejected. 

Such automaton is usually presented as a graph. States ($Q$) are depicted as
nodes (circles), transition function ($\delta$) is drawn as arcs with the input
symbol written above them. Initial state ($g_0$) is marked with a thicker border
or an arrow and each final state (from the set $F$) with a double circle. An
example of such a graph is presented in the figure~\ref{fig:sci:dfa}.

\begin{figure}[htp]
    \centering

    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
        semithick]

        \node[initial,state]   (A)                    {$q_a$};
        \node[state]           (B) [      right of=A] {$q_b$};
        \node[state]           (C) [      right of=B] {$q_c$};
        \node[state,accepting] (D) [      right of=C] {$q_d$};

        \path (A) edge              node [below] {c} (B)
                  edge [loop above]       node {a, t, r} (A)
              (B) edge              node [below] {a} (C)
                  edge [loop above] node {c} (B)
                  edge [bend right] node [above] {t, r} (A)
              (C) edge [bend left]  node {t} (D)
                  edge [bend right] node {r} (D)
                  edge [bend left]  node {a} (A)
                  edge [bend right] node [above] {c} (B)
              (D) edge [loop above] node {a, c, t, r} (D)
    \end{tikzpicture}      
    \caption{An example of a finite state automaton over the alphabet $\{a, c,
             t, r\}$ that accepts all strings containing a word ``cat'' or 
             ``car''.}
\label{fig:sci:dfa}
\end{figure}

The $\delta$ transition function might also be partial, i.e.\ there might exist
some state and symbol for which $\delta$ is undefined. On a graph we will just
not include such arcs. If the machine needs to use such transition, it just
rejects the word instantly and finishes computations. In the
figure~\ref{fig:sci:dfa2} an example of such situation is presented.

\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
                        semithick]

      \node[initial,state]   (A)                    {$q_a$};
      \node[state]           (B) [      right of=A] {$q_b$};
      \node[state]           (C) [      right of=B] {$q_c$};
      \node[state,accepting] (D) [      right of=C] {$q_d$};

      \path (A) edge              node {c} (B)
            (B) edge              node {a} (C)
            (C) edge [bend left]  node {t} (D)
                edge [bend right] node {r} (D)
    \end{tikzpicture}      
    \caption{An example of a finite state automaton that accepts only two strings:
        ``cat'' and ``car''.}
\label{fig:sci:dfa2}
\end{figure}

In the following sections we will build upon this idea and expand it with some
additional features.

\subsection{Non-determinism}\label{sec:sci:wfst:nfa}

The first modification of the finite automata, we will like to introduce, is
non-determinism. The change affects only the transition function $\delta$. In
fact, it would not be a function any more --- it is going to be a relation. What
does that actually change? From one state there can be two outgoing symbols.
Therefore, there may also be two outcomes of reading symbol $s$ in a state $q$.
If that happens, we would take both paths and if any of them finishes in an
accepting state, the whole string is accepted. We could also think about this in
another way: instead of taking two paths we just take the one that will lead to
the success.

\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=0.1cm,
                        semithick]

      \node[initial,state]   (A)                    {$q_0$};
      \node[state,accepting] (B) [      right of=C] {$q_1$};

      \path (A) edge              node {$a$} (B)
            (A) edge [loop above] node {$a$, $n$} (A)
    \end{tikzpicture}      
    \caption{An example of a non-deterministic finite state automaton over the
        alphabet $\{a, n\}$.}
\label{fig:sci:ndfa}
\end{figure}

An example of such machine is presented in the figure~\ref{fig:sci:ndfa}. It
accepts any string that starts with some variable-length variation of $a$ and
$n$ symbols, but it has to end with an $a$ symbol. For example it would accept
words $nana$, $aanaana$, $nananaaaa$, $aaaaaa$, but would not accept $an$,
$aaaaaan$, $nanannan$, etc.

In order to make the automata easier to use, we add one more type of transition
to the $\delta$ relation --- $\varepsilon$-transitions. They can be traversed
without reading any symbols.


\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,
        semithick]

        \node[initial,state] (0)                    	{$q_0$};
        \node[state]         (21) [below left of=0] 	{};
        \node[state]         (31) [below right of=0]        {};
        \node[state,accepting]         (22) [below of=21] 		{};
        \node[state,accepting]         (32) [below right of=31] 		{};
        \node[state,accepting]         (33) [below left of=31] 		{};

        \path (0) edge []  node {$\epsilon$} (21)
        edge []  node {$\epsilon$} (31)
        (21) edge [bend left]  node {$\Sigma$} (22)
        (22) edge [bend left]  node {$\Sigma$} (21)
        (31) edge [bend left]  node {$\Sigma$} (32)
        (32) edge [bend left]  node {$\Sigma$} (33)
        (33) edge [bend left]  node {$\Sigma$} (31);
    \end{tikzpicture}
    \caption{Non-deterministic final state automaton accepting all words of
    a length not divisible by six ($2\cdot3 = 6$).}
\label{fig:sci:ndfa_six}
\end{figure}

Each non-deterministic automaton can be transformed to deterministic one,
that accepts the same language. We call such process \textit{determinisation}.
Unfortunately, sometimes this is very costly. Examine the
figure~\ref{fig:sci:ndfa_six}. It presents an automaton that accepts all words
of length not divisible by $6$. We could construct a similar machine $A_k$ that
accepts all words of length not divisible by a product of all elements of
$\mathcal{P}_k$, where $\mathcal{P}_k$ is a set of first $k$ prime numbers. It
would have $\sum \mathcal{P}_k$ states. After determinisation, its size would
grow to $\prod \mathcal{P}_k$. It is easy to prove by contradiction using the
pigeonhole principle. Therefore, the deterministic automaton recognizing the
same language as a non-deterministic one, can be even exponentially
bigger.



\subsection{Finite state transducers}\label{sec:sci:wfst:fst}

The finite state transducer, instead of having just one input tape, has two
tapes: input and output. During a change of a state, some symbols (even from
different alphabet) might be written to the second one. 

More formally, the \textit{finite state transducer} is a 6-tuple, $(Q, \Sigma_1,
\Sigma_2, \delta, q_0, F)$, where:

\begin{itemize}
    \item $Q$ is a finite set of all states,
    \item $\Sigma_1$ is a finite alphabet, a set of \textbf{input} symbols,
    \item $\Sigma_2$ is also a finite alphabet, a set of \textbf{output} symbols,
    \item $\delta$ ($\delta : Q \times \Sigma_1 \rightarrow Q \times \Sigma_2$) is a transition
        relation,
    \item $q_0$ is an initial state,
    \item $F$ is a set of accepting states.
\end{itemize}


\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
            node distance=2.8cm,semithick]

      \node[initial,state]   (A)                    {$q_a$};
      \node[state]           (B) [      right of=A] {$q_b$};
      \node[state]           (C) [      right of=B] {$q_c$};
      \node[state]           (D) [      right of=C] {$q_d$};
      \node[state,accepting] (E) [      right of=D] {$q_e$};

      \path (A) edge              node {$g$:\textit{graph}} (B)
            (B) edge              node {$r$:$\varepsilon$} (C)
            (C) edge [bend left]  node {\textit{\textipa{A:}}:$\varepsilon$} (D)
                edge [bend right]  node {æ:$\varepsilon$} (D)
            (D) edge              node {$f$:$\varepsilon$} (E)
    \end{tikzpicture}      

    \caption{An example of a finite state transducer.}
\label{fig:sci:fst}
\end{figure}

In the figure~\ref{fig:sci:fst} an example of a finite state transducer is
given. Its input alphabet ($\Sigma_1$) is a set of all phonemes, and the output
alphabet ($\Sigma_2$) is a set of all words. On this example we can see the
first practical usage of FSTs in speech recognition --- the automaton that
reads phonemes and outputs words formed by them.

\subsection{Weighted finite state transducers}\label{sec:sci:wfst:wfst}

The final modification we are going to apply to our automata is the addition of
weights. Each transition and each final state would have some value associated
with it. This values, in our case, would usually be a floating point numbers that
represent probabilities, but generally the weights might represent a variety of
things, e.g.\ distances, costs, etc. The only property that is required from
them is that, together with two binary operations, $\oplus$ and $\otimes$, they
form a semiring. 

When we traverse a path, weights of each arc are multiplied (using $\otimes$
operation). After reaching the final state, the accumulated value is first
multiplied with the state's weight and then the result is added ($\oplus$) to
the results from all other accepting nodes that the machine may finish in.


\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
            node distance=2.8cm,semithick]

      \node[initial,state]   (A)                    {$q_a$};
      \node[state]           (B) [      right of=A] {$q_b$};
      \node[state]           (C) [      right of=B] {$q_c$};
      \node[state]           (D) [      right of=C] {$q_d$};
      \node[state,accepting] (E) [      right of=D] {$q_e$};

      \path (A) edge              node {$g$:\textit{graph}$/1$} (B)
            (B) edge              node {$r$:$\varepsilon/1$} (C)
            (C) edge [bend left]  node {\textit{\textipa{A:}}:$\varepsilon/0.8$} (D)
                edge [bend right]  node {æ:$\varepsilon/0.2$} (D)
            (D) edge              node {$f$:$\varepsilon/1$} (E)
    \end{tikzpicture}      

    \caption{An example of a weighted finite state transducer.}
\label{fig:sci:wfst}
\end{figure}

In the figure~\ref{fig:sci:wfst} an example of a weighted finite state
transducer is given.  It does not only provide a word given its pronunciation,
it also tells us how probable it is.

It turns out that the WFSTs are a very useful tool in the speech recognition. Up
to now, we have been only using them as acceptors, but in fact they are much
more powerful. For example, given a sequence of phonemes, they can generate a
list of all possible words together with associated probabilities.

\subsection{WFST composition}

It is possible to define multiple operations on transducers: concatenation,
union, intersection, negation, etc. For us, the most interesting would be the
\textit{composition}.

It is a binary operation working on transducers $A$ and $B$. If the first one
transduces string $s_1$ to $z_1$ with weight $w_1$ and the second one transduces
$s_2$ to $z_2$ with weight $w_2$ then $A$ composed with $B$ transduces $s_1$ to
$z_2$ with weight $w_1 \otimes w_2$.

Intuitively, one can think that in the beginning the input word is transduced by
the first WFST and then, the outpt is provided as an input for the second one.
The final outcome is the result of second transducer's computation.

\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.5cm,
                        semithick]

      \node[initial,state]   (A)                     {$0$};
      \node[state]           (B) [ above right of=A] {$1$};
      \node[state]           (C) [ below right of=A] {$2$};
      \node[state,accepting] (D) [ below right of=B] {$3$};

      \node[initial,state] (E) [ above right= 1cm and 2cm of D] {$0$};
      \node[state] (F) [ below=1cm of E] {$1$};
      \node[state,accepting] (G) [ below=1cm of F] {$2$};

      \path (A) edge              node {a:1} (B)
                edge              node {c:2} (D)
                edge              node {b:1} (C)
            (B) edge              node {c:2} (D)
                edge [loop above] node {O:\#} (D)
            (C) edge              node {d:2} (D)

            (E) edge              node {1:quick} (F)
            (F) edge              node {2:fox} (G)
            (E) edge [loop right] node {\#:brown} (E)
            (F) edge [loop right] node {\#:brown} (F)
            (G) edge [loop right] node {\#:brown} (G)
            
    \end{tikzpicture}      
    \caption{An example of two weighted finite state transducers.}
\label{fig:sci:wfst_composition1}
\end{figure}



\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3.5cm,
                        semithick]

      \node[initial,state]   (A)                     {$0,0$};
      \node[state]           (B) [ above right of=A] {$1,1$};
      \node[state]           (C) [ below right of=A] {$2,1$};
      \node[state,accepting] (D) [ below right of=B] {$3,2$};

      \path (A) edge              node {a:quick} (B)
                edge [left]       node {b:quick} (C)
            (B) edge              node {c:fox} (D)
                edge [loop above] node {O:brown} (D)
            (C) edge [right]       node {d:fox} (D)
            
    \end{tikzpicture}      
    \caption{A composition of transducers from the
        figure~\ref{fig:sci:wfst_composition1}.}
\label{fig:sci:wfst_composition2}
\end{figure}


In the figures~\ref{fig:sci:wfst_composition1}
and~\ref{fig:sci:wfst_composition2} an example of transducers' composition is
given. The first figure presents two WFSTs and the second one illustrates
the composition of them.

\section{WFSTs and regular expressions}\label{sec:sci:wfsts_and_regexps}

A connection between the \textit{WFSTs} and speech recognition has already been
outlined. We will elaborate on it in Section~\ref{sec:sci:speech}. But what does
it have to do with \textit{regular expressions}? In fact, a lot. Both, regular
expressions and \textit{finite state automata}, recognize the same set of
languages. Even more, for any expression $r$, it is possible to construct an
automaton that identifies $L(r)$. In this section we will show how to do it.

\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=3.5cm,
                        semithick]

      \node[initial,state]   (A1)                {$in$};
      \node[state,accepting] (B1) [ right of=A1] {$out$};
      \node                  (D1) [ right =1.5cm of B1] {expression: $\emptyset$};
      \node[initial,state]   (A2) [ below =1.0cm of A1] {$in$};
      \node[state,accepting] (B2) [ right of=A2] {$out$};
      \node                  (D2) [ right =1.5cm of B2] {expression: $\epsilon$};
      \node[initial,state]   (A3) [ below =1.0cm of A2] {$in$};
      \node[state,accepting] (B3) [ right of=A3] {$out$};
      \node                  (D3) [ right =1.5cm of B3] {expression: $a$};

      \path (A2) edge              node {$\varepsilon$} (B2)
            (A3) edge              node {$a$} (B3)
            
    \end{tikzpicture}  
    \caption{How to construct an automaton representing base regular expression.}
\label{fig:sci:regexp_and_wfst:base}
\end{figure}

Recall the definition of the regular expressions. It was inductive, with three
base symbols and four recursive constructions. The method we will introduce,
defines transducers for base symbols, and then describes a way to connect those
transducers, in order to represent any expression. It is called a
\textit{Thompson's construction algorithm}
\parencite{ken_thompson_programming_1968}.

In the figure~\ref{fig:sci:regexp_and_wfst:base} transducers representing all
base regular expressions are presented.  Note how evident the distinction
between $\emptyset$ and $\varepsilon$  is. It is also important that all of
those transducers have one starting and one accepting state. We will use this
property in the inductive construction. In the description we will assume that
both $A$ and $B$ are WFSTs that represent some regular expressions.


\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
            node distance=1.5cm, 
                        semithick]

      \node[]      (01)                      {};
      \node[state] (A1) [ right of=01]       {$in_1$};
      \node[state] (B1) [ right=1.8cm of A1] {$out_1$};

      \node[state] (A2) [ right=1.5cm of B1] {$in_2$};
      \node[state] (B2) [ right=1.8cm of A2] {$out_2$};
      \node[]      (02) [ right of=B2] {};

      \node[draw,dotted,fit=(A1) (B1),inner sep=0.5em] {$A$};
      \node[draw,dotted,fit=(A2) (B2),inner sep=0.5em] {$B$};

      \path (01) edge              node {} (A1)
            (B2) edge              node {} (02)
            (B1) edge              node {$\epsilon$} (A2) 
            
    \end{tikzpicture}  
    \caption{How to represent a concatenation of two regular expressions as an
             automaton.}
\label{fig:sci:regexp_and_wfst:concatenation}
\end{figure}


The figure~\ref{fig:sci:regexp_and_wfst:concatenation} depicts a contacenation
of $A$ and $B$. We just connect the \textit{out}-node of $A$ with the
\textit{in}-node of $B$ with an $\varepsilon$-transition.

\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
            node distance=1.5cm, 
                        semithick]

      \node[]      (01)                      {};
      \node[state] (02) [ right of=01]       {};

      \node[state] (A1) [ above right=0.6cm and 1.5cm of 02] {$in_1$};
      \node[state] (B1) [ right=1.8cm of A1] {$out_1$};

      \node[state] (A2) [ below right=0.6cm and 1.5cm of 02] {$in_2$};
      \node[state] (B2) [ right=1.8cm of A2] {$out_2$};
      \node[state] (11) [ above right=0.6cm and 1.5cm of B2] {};
      \node[]      (12) [ right of=11] {};

      \node[draw,dotted,fit=(A1) (B1),inner sep=0.5em] {$A$};
      \node[draw,dotted,fit=(A2) (B2),inner sep=0.5em] {$B$};

      \path (01) edge              node {} (02)
            (11) edge              node {} (12)
            (02) edge              node {$\epsilon$} (A1) 
            (02) edge              node {$\epsilon$} (A2) 
            (B1) edge              node {$\epsilon$} (11) 
            (B2) edge              node {$\epsilon$} (11) 
            
    \end{tikzpicture}  
    \caption{How to represent a sum of two regular expressions as an
             automaton.}
\label{fig:sci:regexp_and_wfst:sum}
\end{figure}

The figure~\ref{fig:sci:regexp_and_wfst:sum} presents a sum of
$A$ and $B$. We need to introduce two new states and connect the first one with
both \textit{in}-states and the second one with two \textit{out}-states using an
$\varepsilon$-transitions. That way a decoder might choose any of $A$ and $B$
transducers.

\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,
            node distance=1.5cm, 
                        semithick]

      \node[]      (01)                      {};
      \node[state] (02) [ right of=01]       {};

      \node[state] (A1) [ right=0.6cm and 1.5cm of 02] {$in_1$};
      \node[state] (B1) [ right=1.8cm of A1] {$out_1$};

      \node[state] (11) [ right=0.6cm and 1.5cm of B1] {};
      \node[]      (12) [ right of=11] {};

      \node[draw,dotted,fit=(A1) (B1),inner sep=0.5em] {$A$};

      \path (01) edge              node {} (02)
            (11) edge              node {} (12)
                 edge [bend right] node {$\epsilon$} (02) 
            (02) edge              node {$\epsilon$} (A1) 
                 edge [bend right] node {$\epsilon$} (11)
            (B1) edge              node {$\epsilon$} (11) 
            
    \end{tikzpicture}  
    \caption{How to represent a Kleene star of a regular expression as an
             automaton.}
\label{fig:sci:regexp_and_wfst:kleene}
\end{figure}

The figure~\ref{fig:sci:regexp_and_wfst:kleene} presents a Kleene star of $A$.
Even thought it is an unary operation, the resulting transducer is the most
complicated one, because it generates multiple paths. Besides adding two new
states, input and output, we also need to connect them properly with $A$, so
the decoder can traverse the $A$ graph multiple or zero times.


\section{Speech recognition}\label{sec:sci:speech}

In the following section we will describe some basic concepts of speech
recognition that are essential to know in order to fully understand this work.
It is not aiming to be a complete study of this subject. More information can be
found in any natural language processing textbook,
e.g.~\cite{Jurafsky:2000:SLP:555733}.

The approach to speech recognition we have chosen is based on
\cite{mohri2008speech}. We split the process into four parts: \texttt{H},
\texttt{C}, \texttt{L} and \texttt{G}. Each of them represents a different state
of transformed data. The whole system takes as an input features extracted from
audio data. They may for example consist of \textit{MFCC} and \textit{PLP}
features. Each consecutive part would take as an input the result from its
predecessor.

\begin{description}
    \item[H] part processes the extracted features and outputs context-dependent
        phonemes (one can think about them as phonemes with their surrounding).
    \item[C] represents the context-dependency and given context-dependent
        phonemes outputs context-independent ones.
    \item[L] is a \textit{lexicon}. Provided a list of phonemes, it returns a
        list of words.
    \item[G] is a \textit{language model}. It accepts only sentences that make
        sense in the language in use, with weights representing probabilities of
        such sequence of words.
\end{description}

Each of those parts can be represented as a \textit{WFST}. It is a very useful
property. Thanks to it, almost the whole process of speech recognition can be
implemented as a weighted finite state transducer.

The part that is especially important for us is a \textit{language model} ($G$).
It tells us how probable the given sequence of words is. Usually it is
constructed from a \textit{corpus}, i.e.\ a large set of texts used to
statistically analyse a language.

There are multiple ways to create such models. One of them is an \textit{n-gram
model}. It assigns a probability to each combination of $n$ words. So a
\textit{1-gram} would just be a set of all words with a probability of
occurrence of each of them in the corpus. Analogically, a \textit{2-gram} would
be a set of all combinations of two words, etc. \textit{1-grams} are often
called \textit{unigrams} and \textit{2-grams} --- \textit{bigrams}.

\section{Machine learning}

\textit{Machine learning} is a huge field of science that is evolving rapidly.
In utilizes statistics in order to teach computers to achieve things they were
not explicitly programmed to do. For the purpose of this work we will need to
look only at one specific problem of this field --- \textit{classification}. It
tries to assign one of some predefined categories to each test sample.

\begin{example}[An example of classification problem]
    Given a set of audio recordings and a search pattern, decide if it occurs in
    the data.

    In this problem we have two classes: \textit{positive} and
    \textit{negative}. If a search pattern is in the recording, then it should
    belong to the positive class, otherwise --- to the negative.
\end{example}

A method to solve this problem would be proposed in Chapter~\ref{sec:exp}.
Currently, we will focus on some general techniques to evaluate classifiers.  We
could have just considered general effectiveness of it (how many positive
samples were recognized as positive), but it would not be very informative for
us.  A classifier that always returns positive answer would score $100\%$. So we
need to use something more robust, that would take into the account not only the
number of correctly classified samples, but also the number of incorrectly
reported ones.

In order to do that, let us first consider some options regarding good and bad
classifications. There are actually four options.

\begin{description}
    \item[True positives (TP)] --- samples, that were correctly identified as
        positive.
    \item[False positives (FP)] --- samples, that were incorrectly identified as
        positive.
    \item[True negatives (TN)] --- samples, that were correctly identified as
        negative.
    \item[False negatives (FN)] --- samples, that were incorrectly identified as
        negative.
\end{description}

Actually, it is pretty hard to come up with a method to objectively score a
classifier. It heavily depends on the problem at hand. For example, if we want
to detect cancer, we would rather not have any false negatives, but some false
positives are acceptable. On the other hand, if we have written an algorithm
that controls a rifle and shoots bad people, we would want it to be perfectly
sure that a person is not good before killing them. Therefore, false positives
are not acceptable.

Fortunately, classifiers often have some adjustable parameter, that let us
gradually exchange some false positives by false negatives and vice versa. But
first, we will introduce more definitions.

\begin{description}
    \item[Precision] is a ratio of true positives to all samples that were
        classified as positives. Precision = $TP \over TP + FP$.
    \item[Recall (also Sensitivity, true positive rate (TPR))] is a ratio of
        true positives to all samples, that should be positive. Recall = $TP
        \over TP + FN$.
    \item[Fall-out (also False positive rate (FPR))] is a ratio of false
        positives to all samples, that should be negative. Fall-out = $FP \over
        TN + FP$.
\end{description}

We might use the parameter mentioned in the previous paragraph to run the
classifier multiple times and compare those executions. One of a ways to do that
is a diagram called a \textit{receiver operating characteristic} (\textit{ROC})
diagram. On the $y$ axis it has a true positive rate and on the $x$ axis a false
positive rate. A classifier that just randomly assigns classes to samples with
some probability, that can be adjusted, would always end up on the $y=x$ line.
If some point is below this line, it means that inverting program's decisions
would actually give us better results. We would like our classifier to be as far
to the left and to the top as possible. In the figure~\ref{fig:sci:roc_exmpl} an
example of such ROC diagram is given with the results of four different
classifiers marked on it.


\begin{figure}[htp]
    \centering
    \includegraphics[width=0.7\textwidth]{img/roc_example.png}
    \caption{An example of a ROC diagram with four classifiers marked with
    different colors.}
\label{fig:sci:roc_exmpl}
\end{figure}
