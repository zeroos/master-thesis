% vim:ft=tex:


The main goal of this thesis is to introduce a way to search for phrases in
audio files. Recall Section~\ref{sec:intro:problem}, where the problem has been
formulated. In the following chapter we are going to describe our solution to
this problem.

In Section~\ref{sec:exp:naive} we are going to describe a naive approach that
seems obvious after getting in touch with the problem and speech recognition for
the first time. We will also argue why this solution does not work well. Next,
in Section~\ref{sec:exp:solution}, our solution to the problem will be
described. At the end, in Section~\ref{sec:exp:difficulties}, some difficulties
that came up during the work are outlined.


\section{Naive approach}\label{sec:exp:naive}

\begin{figure}[htp]
    \centering
    \begin{tikzpicture}[->, auto, node distance=6.5cm, thick, text width=3.3cm]

      \node[text width=1.8cm]   (A)                    
      {\includegraphics{img/small_wav.png}};
      \node[]           (B) [right of=A]
      {We have prepared this special offer specially for you.};
      \node[]           (C) [right of=B]
      {We have prepared this \textbf{special} 
       offer \textbf{specially} for you.};

      \path (A) edge node [align=center] {\scriptsize speech recognition} (B)
            (B) edge node [align=center] {\scriptsize searching
                \textit{``special''}} (C);
    \end{tikzpicture}      
  \caption{A naive approach to searching in audio data using speech recognition.}
\label{fig:exp:naive}
\end{figure}

A naive approach that one might think about when given the above problem might
be similar to the one presented in the figure~\ref{fig:exp:naive}. We get the input
audio file and process it using some speech recognition software in order to
obtain text output. Then, we just search for a given phrase in this text using
any standard algorithm. At the end we obtain the same text with occurrences 
of the searched phrase tagged in it. But is it really that simple?

\begin{figure}[]
    \centering
    \begin{tikzpicture}[->, auto, node distance=6.5cm, thick, text width=3.3cm]

      \node[text width=1.8cm]   (A)                    
      {\includegraphics{img/small_wav.png}};
      \node[]           (B) [right of=A]
      {We have repaired this spacial offer the silly for you.};
      \node[]           (C) [right of=B]
      {$\emptyset$};

      \path (A) edge node [align=center] {\scriptsize speech recognition} (B)
            (B) edge node [align=center] {\scriptsize searching
                \textit{``special''}} (C);
    \end{tikzpicture}      

  \caption{Why the naive solution is not working.}
\label{fig:exp:naive-wrong}
\end{figure}

In reality speech recognition software often makes mistakes and recognize
different words than actually spoken. In fact even the best programs struggle in
order to get the word error rate (percentage of incorrectly recognized words)
below $10\%$~\parencite{geoffrey_hinton_deep_2012}. Because of that the
situation might as well look as in the figure~\ref{fig:exp:naive-wrong}. Both
words we were looking for were incorrectly recognized as words that sound very
similar. At the end, the search algorithm did not return any results.

\section{Our solution}\label{sec:exp:solution}

A solution proposed by us, works in a completely different manner. We decided to
examine a way in which speech recognition algorithms work and modify them, such
that we will search for a phrase during speech recognition, not after that. This
effectively merges the two stages (speech recognition and searching) together.

Our idea is based on weighting the \textit{language model} in order to make all
occurrences of the searched pattern more probable. In the
example~\ref{exmpl:exp:our_solution} the intuition how such a model could work 
is presented.

\begin{example}\label{exmpl:exp:our_solution}
    We are given a problem consisting of audio data and a text pattern to find
    in this data. Let's assume there is some speech recognition software that 
    uses both language and acoustic models. For the sake of simplicity let us
    assume the language model is uniform, i.e.\ all words are equally probable. 

    We expect, that the given pattern really exists in audio data, but the
    speech recognition software does not recognize it properly, i.e.\ the
    acoustic model assigns greater probability to some other, similar pattern in
    place where the searched one should occur. 

    But the pattern we are searching for must have also been given high
    probability by the acoustic model, because we assume that acoustic model
    must give high probability to sentences that were actually spoken.

    Now let's consider the same speech recognition software, but with a language
    model that assigns a slightly higher probability to the text pattern we want
    to find. That way even though the phrase we were searching for was not the
    most probable choice of the acoustic model, it will be included in the final
    result.
\end{example}

There are however two things to consider. First, how to implement weighting so
it is efficient and easy to use. Second, how to choose the value by which
phrases should be weighted.


\subsection{How to implement weighting?}

At the beginning of explaining how we have implemented weighting we need to
state the assumptions we have made.

\begin{enumerate}
    \item We do not have any knowledge about the structure of a language model.
        The solution should work with any models.
    \item The goal is to be able to search for multiple phrases at the same
        time.  The reason behind this requirement is that we are often looking
        for a base form of some word and we want to find all occurrences of this
        expression. For example, when searching for \textit{``kill''}, we would
        also like to get results for \textit{``killing''}, \textit{``kills''},
        etc.
    \item Additionally, the phrases we are going to search for, might be of
        variable length, like \textit{``going to school''} or \textit{``Can I
        have three ounces of water please''}. It would be an advantage, if we
        could put placeholders in the middle of sentences, e.g.
        \textit{``Suspect <name> is guilty''}.
\end{enumerate}

Our solution is built upon the idea of representing each step of speech
recognition as a~\textit{WFST} (described in the Section~\ref{sec:sci:wfst})
introduced in~\cite{mohri2008speech} and the regular
expressions as \textit{WFSTs}.

As we know from Section~\ref{sec:sci:wfsts_and_regexps}, any regular expression can be represented as a
\textit{finite state automaton}, which is also a \textit{WFST}. So we introduce
a transducer \texttt{R} that is going to act as the regular expression in the
decoding process. We are going to compose \texttt{R} with the \texttt{HCLG}
graph (described in Section~\ref{sec:sci:speech}).
  
There are two reasonable ways to do that: \texttt{HCRLG} and \texttt{HCLGR}.
The result of both of those compositions is the same, but the input and output
symbols of the regular expression (\texttt{R}) graph are different. In the first
variant, regular expressions would work on phonemes and output phonemes. This is
a pretty intuitive way to define them. However, the second variant (with
\texttt{R} at the end) has a useful property~--- it lets us define the output
symbols any way we want. Finally, we decided to use the \texttt{HCLGR} solution.
Thanks to that we were able to mark the found occurrences with special symbols
that let us easily localize the place where the regular expression was found.

\begin{figure}[htp]
    \centering
    \includegraphics[width=\textwidth]{img/regexp_findall.png}
\caption{An example of WFST that is used to find all occurrences of phrase
    ``\myregexp{ABC}'' in a text over the alphabet \{A, B, C\}. In order to
    make it more readable it is not minimized and thus contains some
    unnecessary \textit{<eps>} arcs. Note the arc
    from state $6$ to state $0$ that may be used in order to traverse the
    path with regular expression multiple times.}
\label{fig:exp:findall_regexp}
\end{figure}


\begin{figure}[htp]
\centering
\includegraphics[width=\textwidth]{img/regexp_find.png}
\caption{An example of WFST that is used to find just one occurrence of the
    regular expression ``\myregexp{ABC}'' in a text over the alphabet \{A,
    B, C\}. In order to make it more readable it is not minimized and thus
    contains some unnecessary \textit{<eps>} arcs.}
\label{fig:exp:find_regexp}
\end{figure}

In the figures~\ref{fig:exp:findall_regexp} and~\ref{fig:exp:find_regexp} two
kinds of \texttt{R} graphs implemented by us have been presented. The first one
is used to find all occurrences of the given phrase. It reduces the weight of
associated regular expression, so it is more probable to find it in the audio
data. The second one, on the other hand, ensures that the phrase is found
somewhere in the recording. It might be useful, for example, when we know that
something has been said and we want to precisely locate it in the input data.

\subsection{How to choose the regular expression's weight value?}\label{sec:exp:solution:choosing_weight}

The last thing that is left, is to choose the weight value, i.e.\ the cost of
the arcs that represent the search phrase. After the regular expression is
composed with an \texttt{HCLG} graph, this value would be multiplied ($\otimes$)
by appropriate arc's weights in the language model, effectively reducing the
traversal cost for those words.

\begin{example}[Decoding of the same recording with different weights]\hfill \\
\begin{description}
    \item[transcription:] kolejne wartości kolejne ideały które miejmy
        nadzieję że będą przyświecać poszczególnym pokoleniom

    \item[weight $0$:] p a l e j n e w a r a t o si ci i k o l e ni a i d e
        ng a ll e k t u l e ni e j n e n a dzi e rz e w e n a m p sz y si f
        j e c a ci p o cz e b o l e m p o k o l e ni o

    \item[weight $-2$:] p a l e j n e w a r a t o si ci i k o l e ni a I D E A
        <re\_found> ll e p ll n e m j e n e n a dzi e rz e w e n a m p sz y si f
        j e c a ci p o c z e b o l e m o k o l e ni o
\end{description}


\label{exmpl:exp:decoding}
\end{example}


In the example~\ref{exmpl:exp:decoding} we have presented an output of the
speech recognition algorithm for the same input, but with different regular
expression weights. The phrases to look for in both cases are identical and
represent all forms of the word ``idea'' in Polish.  All occurrences of the
searched phrase are marked with upper-case letters and ``<re\_found>'' symbol
appended just after them. The search pattern was present in the test sentence,
but it was not found when the weight of the regular expression was $0$. Changing
it to $-2$ allowed locating it in the correct place, but in a slightly
different form.

Why is this parameter so significant and how should it be set? It depends on the
situation. Sometimes, when it is not important to find each occurrence of the
pattern, but we do not want to tag some data incorrectly, it would be better to
use a greater weight value. On the other hand, when it is essential to locate the phrase
each time it appeared and some additional incorrect detections are tolerable the
value of this parameter should be small. Later it might be possible to further
filter out the false results using different algorithms or sometimes even
manually.

Because this parameter has such a big impact on results and it depends so much
on a use-case, we decided to let the user adjust it. If we needed to construct a
general algorithm and specify a default value for this weight, we would probably
pick the one with the greatest \textit{F1~score} on a training data.

\section{Difficulties}\label{sec:exp:difficulties}

During the development of this project we have often found ourselves in a place,
where theory seems to work very well, the input data looks good, but the
algorithm does not compute any answer at all or produces unsatisfactory results.
Often those problems were caused by the \textit{FSTs} growing rapidly. For
example, during determinization, automata usually grow quadratically fast. 

\begin{figure}[htp]
    \centering
    \includegraphics[page=1,width=\textwidth]{img/impossible_automata.png}
    \caption{An example of \textit{WFST} that represents regular expression
        ``\myregexp{.*((NA)+)*.*}''. }
\label{fig:exp:impossible_automata}
\end{figure}

In fact, some transducers cannot be determinized at all. In the
figure~\ref{fig:exp:impossible_automata} an example of such WFST is given.
It represents a very easy regular expression: ``\myregexp{.*((NA)+)*.*}''. Any
equal determenistic transducer cannot exist, because the same input word can
generate two different outputs. The word \myregexp{NA} can be transformed to
\myregexp{NA} with weight $-3$ (using all states), or $0$ (by looping in the
state $0$).

Fortunately, the determinization of \texttt{HCLGR} graph is only needed to speed
up the process of decoding. However, we did not want to skip it completely. The
final solution determinizes the regular expression before adding \myregexp{.*}
parts around it. The examples of graphs obtained using this technique will be
presented in the next chapter.
