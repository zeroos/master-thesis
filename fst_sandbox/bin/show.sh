#! /bin/sh

set -e

dir=`dirname $0`
tmp=`mktemp`
dotfile=${tmp}.dot
psfile=${tmp}.ps
fstfile=$1
isyms=${2:-"$dir/../fsts/num.syms"} 
osyms=${3:-$isyms} 

fstdraw --isymbols=$isyms --osymbols=$osyms $fstfile $dotfile 
xdot $dotfile
#dot -Tps $dotfile > $psfile
#okular $psfile 2> /dev/null
rm $dotfile $psfile $tmp


