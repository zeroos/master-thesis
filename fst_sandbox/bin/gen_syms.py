#! /usr/bin/env python


def n2s(num):
    """ converts number to string """
    if chr(num) == "'":
        return "'"

    result = repr(chr(num)).strip("'")
    if result == " ":
        return "<space>"
    if result == "":
        return "<unk>"
    if result == "\\\\":
        return "\\"
    return result


def main():
    zero_line = "<eps> 0\n"
    lines = [zero_line] + ["{} {}\n".format(n2s(i), i) for i in range(1, 256)]
    f = open("fsts/M.isyms", 'w')
    f.writelines(lines)
    f.close()
    f = open("fsts/M.osyms", 'w')
    f.writelines(lines)
    f.close()


if __name__ == "__main__":
    main()
