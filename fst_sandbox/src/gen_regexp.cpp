#include <iostream>
#include <fstream>
#include <fst/fstlib.h>
#include <map>
#include <vector>
#include <boost/algorithm/string.hpp>

//#define debug

using namespace std;
using namespace fst;

typedef StdVectorFst NDFA;
typedef map<string, int> alphabet_t;
typedef vector<int> re_t;
typedef StdArc::StateId StateId;


NDFA regexp(re_t &regexp, alphabet_t &alphabet);
NDFA term(re_t &regexp, alphabet_t &alphabet);
NDFA repetitions(re_t &regexp, alphabet_t &alphabet);
NDFA base(re_t &regexp, alphabet_t &alphabet);
NDFA character(re_t &regexp, alphabet_t &alphabet);

const int RE_STAR = -1; //*
const int RE_PLUS = -2; //+
const int RE_OR = -3;   //|
const int RE_GROUP_BEGIN = -4; //(
const int RE_GROUP_END = -5; //)
const int RE_WORD_GROUP_BEGIN = -6; //[
const int RE_WORD_GROUP_END = -7; //[
const int RE_ANY_CHAR = -8; //.

double EXTERNAL_ARC_WEIGHT = 0; // weight of an arc outside regexp (eg. in find mode)
double INTERNAL_ARC_WEIGHT = -1.5; // weight of an arc inside regexp
double NEUTRAL_ARC_WEIGHT = 0;

void print_regexp(re_t r) {
    cout << "[";
    for(re_t::iterator it=r.begin(); it!=r.end(); it++){
        cout << " " << *it << " ";
    }
    cout << "]";
}

void print_ndfa(NDFA fst) {
    cout << "{";
    for (StateIterator<StdFst> siter(fst); !siter.Done(); siter.Next()) {
        StateId state_id = siter.Value();
        cout << state_id << " (";
        for (ArcIterator<StdFst> aiter(fst, state_id); !aiter.Done(); aiter.Next()){
            const StdArc &arc = aiter.Value();
            cout << " " << arc.weight << " ";
        }
        cout << "), ";
    }
    cout << "}";
}

NDFA regexp(re_t &regexp, alphabet_t &alphabet) {
    #ifdef debug
        cout << "regexp('";
        print_regexp(regexp);
        cout << "')" << endl;
    #endif
    NDFA ndfa = term(regexp, alphabet);
    while(regexp.size() > 0 && regexp.at(0) == RE_OR){
        regexp.erase(regexp.begin());
        NDFA ndfa_opt = term(regexp, alphabet);

        Union(&ndfa, ndfa_opt);
    }
    #ifdef debug
        cout << "Left: '";
        print_regexp(regexp);
        cout << "'" << endl;
    #endif
    RmEpsilon(&ndfa);
    #ifdef debug
        cout << "Removed epsilons." << endl;
    #endif
    return ndfa;
}

NDFA term(re_t &regexp, alphabet_t &alphabet) {
    #ifdef debug
        cout << "term('";
        print_regexp(regexp);
        cout << "')" << endl;
    #endif

    NDFA ndfa;
    ndfa.AddState();
    ndfa.SetStart(0);
    ndfa.SetFinal(0, 0);
    while(regexp.size() > 0 && regexp.at(0) != RE_OR && 
            regexp.at(0) != RE_GROUP_END) {
        Concat(&ndfa, repetitions(regexp, alphabet));
    }
    return ndfa;
}

NDFA repetitions(re_t &regexp, alphabet_t &alphabet) {
    #ifdef debug
        cout << "repetitions('";
        print_regexp(regexp);
        cout << "')" << endl;
    #endif

    NDFA ndfa = base(regexp, alphabet);

    while(regexp.size() > 0 && 
            (regexp.at(0) == RE_STAR || regexp.at(0) == RE_PLUS)){
        if(regexp.at(0) == RE_STAR){
            Closure(&ndfa, CLOSURE_STAR);
        }else if(regexp.at(0) == RE_PLUS){
            Closure(&ndfa, CLOSURE_PLUS);
        }
        regexp.erase(regexp.begin());
    }
    return ndfa;
}

NDFA base(re_t &regex, alphabet_t &alphabet) { 
    #ifdef debug
        cout << "base('";
        print_regexp(regex);
        cout << "')" << endl;
    #endif

    if(regex[0] == RE_GROUP_BEGIN){
        regex.erase(regex.begin());
        NDFA ndfa = regexp(regex, alphabet);
        assert(regex[0] == RE_GROUP_END);
        regex.erase(regex.begin());
        return ndfa;
    }else{
        
        return character(regex, alphabet);
    }
}

NDFA character(re_t &regex, alphabet_t &alphabet){
    #ifdef debug
        cout << "character('";
        print_regexp(regex);
        cout << "')" << endl;
    #endif


    NDFA ndfa;

    ndfa.AddState();
    ndfa.SetStart(0);
    ndfa.AddState();
    if(regex[0] == RE_ANY_CHAR) {
        for(alphabet_t::iterator it=alphabet.begin(); it != alphabet.end();
                it++){
            ndfa.AddArc(0, StdArc((*it).second, (*it).second, INTERNAL_ARC_WEIGHT, 1));
        }
    }else if(regex[0] == RE_WORD_GROUP_BEGIN) {
        regex.erase(regex.begin());
        while(regex[0] != RE_WORD_GROUP_END){
            ndfa.AddArc(0, StdArc(regex[0], regex[0], INTERNAL_ARC_WEIGHT, 1));
            regex.erase(regex.begin());
        }   
    }else if(regex[0] == '\\') { //not used anymore as the text is
                                 //pre-processed. Left as an example.
        regex.erase(regex.begin());
        ndfa.AddArc(0, StdArc(regex[0], regex[0], INTERNAL_ARC_WEIGHT, 1));
    }else{
        ndfa.AddArc(0, StdArc(regex[0], regex[0], INTERNAL_ARC_WEIGHT, 1));
    }
    regex.erase(regex.begin());
    ndfa.SetFinal(1, 0);
    return ndfa;
}

NDFA regex_find(alphabet_t &alphabet, alphabet_t &enriched_alphabet, NDFA ndfa,
        bool mark_found=false){
    /* transforms a regex to a "find" regex, ie. appends .* at the beginning
     * and end of the regex 
     */
    NDFA S;
    NDFA E;
    S.AddState();
    E.AddState();
    E.AddState();
    S.SetStart(0);
    E.SetStart(0);

    if(mark_found){
        E.AddArc(0, StdArc(0, //epsilon
                           enriched_alphabet["<re_found>"], 
                           NEUTRAL_ARC_WEIGHT, 
                           1));
    }else{
        E.AddArc(0, StdArc(0, //epsilon
                           0,
                           NEUTRAL_ARC_WEIGHT, 
                           1));
    }

    for(alphabet_t::iterator it=alphabet.begin(); it != alphabet.end(); it++){
        S.AddArc(0, StdArc((*it).second, 
                    enriched_alphabet[(*it).first], 
                    EXTERNAL_ARC_WEIGHT, 
                    0));
        E.AddArc(1, StdArc((*it).second, 
                    enriched_alphabet[(*it).first], 
                    EXTERNAL_ARC_WEIGHT, 
                    1));

    }
    S.SetFinal(0, 0);
    E.SetFinal(1, 0);
 
    RmEpsilon(&ndfa);

    NDFA D;
    NDFA T;
    //Difference(S, ndfa, &T);
    //ArcSort(&T, StdOLabelCompare());
    //ArcSort(&E, StdILabelCompare());
    //return T;
    //Compose(T, E, &D);

    NDFA result = S;
    Concat(&result, ndfa);
    Concat(&result, E);

    result.SetFinal(0,0);
    return result;
}

string enriched_symbol(string old_sym) {
    if(old_sym == "<eps>") {
        return old_sym;
    }
    string sym = boost::to_upper_copy(old_sym);
    if(sym == old_sym) {
        sym += "'";
    }
    return sym;
}

void generate_enriched_alphabet(alphabet_t &alphabet, alphabet_t &new_alphabet) {
    int max_id = 0;
    for(alphabet_t::iterator it=alphabet.begin(); it != alphabet.end(); it++){
        string sym = enriched_symbol(it->first);
        new_alphabet[sym] = it->second;
        if(it->second > max_id) {
            max_id = it->second;
        }
    }
    for(alphabet_t::iterator it=alphabet.begin(); it != alphabet.end(); it++){
        if(it->first == "<eps>") continue;
        new_alphabet[it->first] = ++max_id;
    }
    new_alphabet["<re_found>"] = ++max_id;
}

void save_alphabet(char * filename, alphabet_t &alphabet) {
    cout << "Saving new alphabet to " << filename << endl;
    ofstream f(filename);
    for(alphabet_t::iterator it=alphabet.begin(); it != alphabet.end(); it++){
        f << it->first << " " << it->second << endl;
    }
    cout << "Saved." << endl;
}

void load_alphabet(char * filename, alphabet_t *alphabet) {
    ifstream f(filename);
    string key;
    int id;
    while(f){
        f >> key >> id;
        if(id < 0) {
            cout << "[ERROR] Symbol keys cannot have negative ids." << endl;
            throw 1;
        }
        (*alphabet)[key] = id;
    }
}

void encode_input(char * input_filename, re_t *re, alphabet_t *alphabet) {
    cout << "Encoding regexp from file '" << input_filename << "'" << endl;
    ifstream f(input_filename);
    while(f) {
        string s;
        f >> s;
        if(s.length() == 0 && s[0] == '\0'){
            continue;
        }
        if(s == "*"){
            re->push_back(RE_STAR);
            continue;
        }else if(s == "+"){
            re->push_back(RE_PLUS);
            continue;
        }else if(s == "|"){
            re->push_back(RE_OR);
            continue;
        }else if(s == "("){
            re->push_back(RE_GROUP_BEGIN);
            continue;
        }else if(s == ")"){
            re->push_back(RE_GROUP_END);
            continue;
        }else if(s == "["){
            re->push_back(RE_WORD_GROUP_BEGIN);
            continue;
        }else if(s == "]"){
            re->push_back(RE_WORD_GROUP_END);
            continue;
        }else if(s == "."){
            re->push_back(RE_ANY_CHAR);
            continue;
        }else if(s == "\\"){
            f >> s;
        }
        if(alphabet->find(s) == alphabet->end()) {
            cout << "Symbol '" << s << "'" <<
                " not found in the alphabet. Ignoring it." << endl;
            continue;
        }
        re->push_back((*alphabet)[s]);
    }
}


//http://stackoverflow.com/questions/865668/how-to-parse-command-line-arguments-in-c
char* getCmdOption(char ** begin, char ** end, const std::string & option)
{
    char ** itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        return *itr;
    }
    return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option)
{
    return std::find(begin, end, option) != end;
}



int main(int argc,  char** argv) {
    if(argc < 4){
        cout << "Usage: " << endl;
        cout << "    " << argv[0] << " regexp_file syms_file out_file [OPTION]" << endl;
        cout << endl;
        cout << "Available options:" << endl;
        cout << endl;
        cout << "    -findall               outputs a fst that matches .*(regexp)*.* with symbols in regexp 'enriched'" << endl;
        cout << "    -find                  outputs a fst that matches .*regexp.* with symbols in regexp 'enriched'" << endl;
        cout << endl;
        cout << "    -minimize              minimizes the fst after creating it" << endl;
        cout << "    -minimize_internal     if used with find* options, minimizes only the regexp fst, not the whole fst" << endl;
        cout << endl;
        cout << "    -internal_weight W     all arcs inside regexp would have weight W" << endl;
        cout << "    -external_weight W     all arcs outside regexp would have weight W (eg. in find mode)" << endl;
        cout << "    -mark_found            mark the end of the regexp with '<re_found>' symbol" << endl;
        exit(1);
    }


    char * w = getCmdOption(argv, argv + argc, "-external_weight");
    if(w != 0) {
        EXTERNAL_ARC_WEIGHT = atof(w);
        cout << "External arc weight set to " << EXTERNAL_ARC_WEIGHT << endl;
    }

    w = getCmdOption(argv, argv + argc, "-internal_weight");
    if(w != 0) {
        INTERNAL_ARC_WEIGHT = atof(w);
        cout << "Internal arc weight set to " << INTERNAL_ARC_WEIGHT << endl;
    }

    cout << "Loading syms from '" << argv[2] << "'" << endl;
    alphabet_t alphabet;

    load_alphabet(argv[2], &alphabet);

    re_t re;
    encode_input(argv[1], &re, &alphabet);

    //cout << "Processing regexp: '" << re << "'" << endl;
    cout << "Processing regexp: ";
    print_regexp(re);
    cout << endl;
    NDFA model = regexp(re, alphabet);


    if(cmdOptionExists(argv, argv+argc, "-minimize_internal")) {
        cout << "Minimizing internal fst..." << endl;
        NDFA tmp;
        Determinize(model, &tmp);
        model = tmp;
        Minimize(&model);
    }


    if(cmdOptionExists(argv, argv+argc, "-findall") || cmdOptionExists(argv, argv+argc, "-find")) {
        bool mark_found = cmdOptionExists(argv, argv+argc, "-mark_found");
        cout << "Applying findall/find..." << endl;
        char out_filename[800];
        alphabet_t enriched_alphabet;
        generate_enriched_alphabet(alphabet, enriched_alphabet);

        strcpy(out_filename, argv[3]);
        strcat(out_filename, ".osyms");
        save_alphabet(out_filename, enriched_alphabet);

        model = regex_find(alphabet, enriched_alphabet, model, mark_found);
        if(cmdOptionExists(argv, argv+argc, "-findall")) {
            Closure(&model, CLOSURE_STAR);
        }
    }

    if(cmdOptionExists(argv, argv+argc, "-minimize")) {
        cout << "Minimizing..." << endl;
        NDFA a;
        Determinize(model, &a);
        model = a;
        Minimize(&model);
    }
    cout << "Saving..." << endl;
    model.Write(argv[3]);
    cout << "Success!" << endl;
    return 0;
}
