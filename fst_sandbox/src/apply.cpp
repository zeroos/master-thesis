#include <iostream>
#include <fstream>
#include <fst/fstlib.h>
#include <boost/algorithm/string/predicate.hpp>

#define MAX_F_SIZE 4096

using namespace std;
using namespace fst;

int main(int argc,  char** argv) {
    if(argc < 3){
        cout << "Usage: " << endl;
        cout << "    " << argv[0] << " model.fst input.fst" << endl;
        cout << "    " << argv[0] << " model.fst input.txt" << endl;
        cout << "    " << argv[0] << " model.fst \"Input string\"" << endl;
        exit(1);
    }
    StdVectorFst *model = StdVectorFst::Read(argv[1]);
    StdVectorFst *input;

    if(boost::algorithm::ends_with(argv[2], ".fst")) {
         input = StdVectorFst::Read(argv[2]);
    }else{
        char txt[MAX_F_SIZE];
        if(boost::algorithm::ends_with(argv[2], ".txt")) {
            ifstream f(argv[2]);

            f.read(txt, MAX_F_SIZE);
        }else{
            strcpy(txt, argv[2]);
        }
        cout << txt << endl;
        input = new StdVectorFst();
        input->AddState();
        input->SetStart(0);
        int i;
        for(i=0; txt[i] != '\0'; i++) {
            input->AddState();
            input->AddArc(i, StdArc(txt[i], txt[i], 1, i+1));
        }
        input->SetFinal(i, 1);
    }

    ArcSort(input, StdOLabelCompare());
    ArcSort(model, StdILabelCompare());

    StdVectorFst result;

    Compose(*input, *model, &result);

    result.Write("result.fst");   
    return 0;
}
