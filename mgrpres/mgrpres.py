#! /usr/bin/env python
import os
from subprocess import call

from base_forms import parse_base_forms, data2ascii

SAY_SENTENCE_CMD = ["/usr/bin/python2", "/home/zeroos/uni/magisterka/" +
                    "saySentence/gen_transcripts.py"]
GEN_REGEXP_CMD = ["/home/zeroos/uni/magisterka/fst_sandbox/bin/gen_regexp"]
SYMS_FILE = "phones.txt"


def gen_regexps(text, weight=-0.33, data={}):
    if text in data:
        values = data[text]
    else:
        values = [text]

    dirname = "tmp/{}".format(data2ascii(text))
    try:
        os.makedirs(dirname)
    except FileExistsError:
        pass
    textfile = os.path.join(dirname, "text")
    saidfile = os.path.join(dirname, "said")
    textregexpfile = os.path.join(dirname, "regexp.txt")
    regexpfile = os.path.join(dirname, "regexp.fst")

    with open(textfile, "w") as f:
        f.writelines(("{}\n".format(v) for v in values))

    call(SAY_SENTENCE_CMD + [textfile, saidfile])

    with open(saidfile) as f:
        with open(textregexpfile, "w") as f2:
            f2.write(' | '.join([a.strip() for a in f.readlines()]))

    call(GEN_REGEXP_CMD + [textregexpfile, SYMS_FILE,
                            regexpfile, "-findall",
                            "-minimize_internal",
                            # "-mark_found",
                            "-internal_weight", str(weight)])
    return (regexpfile, "{}.osyms".format(regexpfile))


def scp(filenames):
    call(["scp"] + filenames + ["cymes:~"])


def main():
    data = parse_base_forms()

    text = input("Podaj wyszukiwane wyrażenie: ")
    print("Nagraj się")
    try:
        call(["arecord", "-d", "10", "-r", "16000", "-t", "wav", "-f", "s16_le", "/tmp/nagranie.wav"])
    except KeyboardInterrupt:
        pass

    print()
    print("Przygotowuje fst...")
    fst_file, osyms_file = gen_regexps(text, -2, data)

    print("Kopiuję pliki  na serwer...")
    scp([fst_file, osyms_file, "/tmp/nagranie.wav"])
    print("Dekoduję nagranie...")
    call(["ssh", "i254278@mbarcis.net", "-p", "12345", "bash run_mgrpres.sh"])
    #call(["ssh", "cymes", "bash run_mgrpres.sh"])


if __name__ == "__main__":
    main()
